/*
 * mtdev2tuio
 *
 * Copyright (C) 2010, 2012 Richard Nauber <Richard.Nauber@gmail.com>
 *
 * This tool is based on the excellent work of:
 * liblo
 * 	Copyright (C) 2004 Steve Harris, Uwe Koloska (LGPL)
 * mtdev - Multitouch Protocol Translation Library (MIT license)
 * 	Copyright (C) 2010 Henrik Rydberg <rydberg@euromail.se>
 * 	Copyright (C) 2010 Canonical Ltd.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

/* TODO:
	- calculate acceleration
	- make TUIO port and address configurable
	- implement pressure event(?)
	- add a ctrl-c handler
*/

#include "lo/lo.h"
#include <mtdev.h>
#include <mtdev-mapping.h>

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

#define MAXSLOTS 8

#define DEBUG
//#define DEBUGV

#ifdef DEBUG
#define DBG(x) printf x
#else
#define DBG(x)
#endif

#ifdef DEBUGV
#define DBGV(x) printf x
#else
#define DBGV(x)
#endif

#define NSEC_PER_USEC   1000L
#define NSEC_PER_SEC    1000000000L

static inline __u64 timeval_to_ns(const struct timeval *tv)
{
	return ((__u64) tv->tv_sec * NSEC_PER_SEC) +
	    tv->tv_usec * NSEC_PER_USEC;
}

typedef __u64 nstime;

struct slotdata_t {
	/* current sample */
	float x, y;
	float dx, dy;

	/* previous samples */
	float x_1, y_1;
	nstime tx_1, ty_1;

	int id;
};

struct state_t {
	/* frame id */
	unsigned int fid;

	/* address of out tuio client*/
	lo_address tuioaddr;

	/* per slot information */
	struct slotdata_t sldata[MAXSLOTS];

	/* current slot */
	int cs;		

	/*normalisation values */
	int x_ofs, y_ofs;
	float x_scale, y_scale;
};

static float calc_speed(float s, float s_1, nstime t, nstime t_1)
{
	return ((s - s_1) * (float)NSEC_PER_SEC / (t - t_1));
}

static void send_msg_set(struct state_t *s, int slot)
{

	if (lo_send
	    (s->tuioaddr, "/tuio/2Dcur", "sifffff", "set",
	     s->sldata[slot].id, s->sldata[slot].x,
	     s->sldata[slot].y, s->sldata[slot].dx, s->sldata[slot].dy,
	     0.0) == -1) {
		printf("OSC error %d: %s\n", lo_address_errno(s->tuioaddr),
		       lo_address_errstr(s->tuioaddr));
	}

	DBG((" set %i: x=%f y=%f dx=%f dy=%f\n", s->sldata[s->cs].id,
	     s->sldata[slot].x, s->sldata[slot].y, s->sldata[slot].dx,
	     s->sldata[slot].dy));
}

static void send_msg_alive(struct state_t *s)
{
	lo_message msg_alive;
	int i;

	msg_alive = lo_message_new();
	lo_message_add_string(msg_alive, "alive");

	for (i = 0; i < MAXSLOTS; i++) {
		if (s->sldata[i].id != -1) {
			lo_message_add_int32(msg_alive, s->sldata[i].id);
			DBGV((" id=%i ", s->sldata[i].id));
		}
	}

	DBGV(("\n"));

	lo_send_message(s->tuioaddr, "/tuio/2Dcur", msg_alive);
	lo_message_free(msg_alive);

	if (lo_send(s->tuioaddr, "/tuio/2Dcur", "si", "fseq", s->fid++) == -1) {
		printf("OSC error %d: %s\n",
		       lo_address_errno(s->tuioaddr),
		       lo_address_errstr(s->tuioaddr));
	}

}

static void process_event(struct state_t *s, const struct input_event *ev)
{
	unsigned int i;
	nstime time;

	if (ev->type == EV_ABS && ev->code == ABS_MT_SLOT)
		s->cs = ev->value;

	DBGV(("%02d %01d %04x %d\n", s->cs, ev->type, ev->code, ev->value));

	if (ev->type == EV_ABS) {

		switch (ev->code) {
		case ABS_MT_TRACKING_ID:
			s->sldata[s->cs].id = ev->value;
			break;

		case ABS_MT_POSITION_X:
			s->sldata[s->cs].x_1 = s->sldata[s->cs].x;
			s->sldata[s->cs].x =
			    (ev->value - s->x_ofs) * s->x_scale;
			time = timeval_to_ns(&ev->time);

			s->sldata[s->cs].dx =
			    calc_speed(s->sldata[s->cs].x, s->sldata[s->cs].x_1,
				       time, s->sldata[s->cs].tx_1);
			s->sldata[s->cs].tx_1 = time;

			break;

		case ABS_MT_POSITION_Y:

			s->sldata[s->cs].y_1 = s->sldata[s->cs].y;
			s->sldata[s->cs].y =
			    (ev->value - s->y_ofs) * s->y_scale;
			time = timeval_to_ns(&ev->time);

			s->sldata[s->cs].dy =
			    calc_speed(s->sldata[s->cs].y, s->sldata[s->cs].y_1,
				       time, s->sldata[s->cs].ty_1);
			s->sldata[s->cs].ty_1 = time;

			/* send "set" message for the current slot */
			send_msg_set(s, s->cs);
			break;

		default:
			break;
		}
	}

	if (ev->type == EV_SYN && ev->code == SYN_REPORT) {
		/* send "alive" message for all active slots */
		send_msg_alive(s);

	}

}

static void init_state(struct mtdev *dev, struct state_t *s)
{
	unsigned int i;

	/* calculate scale factors */

	s->x_ofs = dev->caps.abs[MTDEV_POSITION_X].minimum;
	s->y_ofs = dev->caps.abs[MTDEV_POSITION_Y].minimum;

	s->x_scale =
	    1.0f / (dev->caps.abs[MTDEV_POSITION_X].maximum -
		    dev->caps.abs[MTDEV_POSITION_X].minimum);
	s->y_scale =
	    1.0f / (dev->caps.abs[MTDEV_POSITION_Y].maximum -
		    dev->caps.abs[MTDEV_POSITION_Y].minimum);

	DBG(("x=%i-%i y=%i-%i\n", dev->caps.abs[MTDEV_POSITION_X].minimum,
	     dev->caps.abs[MTDEV_POSITION_X].maximum,
	     dev->caps.abs[MTDEV_POSITION_Y].minimum,
	     dev->caps.abs[MTDEV_POSITION_Y].maximum));

	/* mark all slots as unused */
	for (i = 0; i < MAXSLOTS; i++) {
		s->sldata[i].id = -1;
	}

	s->cs = 0;

}

int main(int argc, char *argv[])
{
	int fd;
	struct mtdev dev;
	struct state_t state;
	struct input_event ev;

	if (argc < 2) {
		fprintf(stderr, "Usage: mtdev2tuio <device>\n");
		return -1;
	}
	fd = open(argv[1], O_RDONLY | O_NONBLOCK);
	if (fd < 0) {
		fprintf(stderr, "error: could not open device\n");
		return -1;
	}
	if (ioctl(fd, EVIOCGRAB, 1)) {
		fprintf(stderr, "error: could not grab the device\n");
		return -1;
	}

	if (mtdev_open(&dev, fd)) {  
		fprintf(stderr, "error: could not open device!\n");
		return;
	}

	/* initialize */
	init_state(&dev, &state);

	/* set the tuio address */
//      tuioaddr = lo_address_new_from_url( "osc.unix://localhost/tmp/mysocket" );
	state.tuioaddr = lo_address_new(NULL, "3333");

	/* process all available events */
	while (1) {
		while (mtdev_get(&dev, fd, &ev, 1) > 0) {
			process_event(&state, &ev);
		}
	}

	/* this is not executed yet ! */
	mtdev_close(&dev);

	ioctl(fd, EVIOCGRAB, 0);
	close(fd);
	return 0;
}
